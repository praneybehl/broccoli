export const apiURI = 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth';
export const validationErrors = {
    fName: 'Full Name required',
    email: 'Invalid email',
    email2: 'Email doesn\'t match. Try again...',
    multiple: 'Please fill in the missing fields'
};

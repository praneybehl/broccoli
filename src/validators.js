const  emailRE = new RegExp('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');

export const validateEmail = value => emailRE.test(String(value).toLowerCase());
export const validateEmailConfirm = (email, email2) => email && email2 && email === email2;
export const validateNotEmpty = value => value && value.length > 3;

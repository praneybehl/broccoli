import React, { Component } from  'react'
import {validateEmail, validateNotEmpty, validateEmailConfirm} from '../../validators'
import { apiURI, validationErrors } from '../../constants'
import fetch from '../../fetch'
import { AppContext } from '../../AppContext'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import HeroIntro from '../HeroIntro/HeroIntro'
import Overlay from '../Overlay/Overlay'
import SubscribeForm from '../SubscribeForm/SubscribeForm'

class App extends Component {
    state = {
        overlayVisible : false,
        formError: null,
        isSending: false,
        formCtaText: "Send",
        formSendingText: "Sending, please wait...",
        formSuccess: false,
        formTitleText: "Request an Invite",
        formTitleSuccessText: "All Done!",
        formFullName: "",
        formEmail: "",
        formEmailConfirm: ""
    }

    render() {
        return (
            <AppContext.Provider value={{
                state: this.state,
                handleOverlayClose: () => this.setState({
                    overlayVisible: false
                }),
                handleOverlayOpen: () => this.setState({
                    overlayVisible: true
                }),
                updateInputField: (e) => {
                    const name = e.target.name;
                    const value = e.target.value;
                    switch(name) {
                        case 'full_name':
                            return this.setState({ formFullName: value });
                        case 'email':
                            return this.setState({ formEmail: value });
                        case 'email_confirm':
                            return this.setState({ formEmailConfirm: value });
                        default:
                    }
                },
                handleFormSubmit: (e) => {
                    e.preventDefault();
                    // this.validateForm(e)
                    const {formFullName, formEmail, formEmailConfirm} = this.state;
                    let errorCount = 0;
                    if(!validateNotEmpty(formFullName)) {
                        e.target.querySelector('#full_name').classList.add('invalid');
                        errorCount += 1;
                        this.setState({formError: validationErrors.fName});
                    }
                    else  e.target.querySelector('#full_name').classList.remove('invalid');

                    if(!validateEmail(formEmail)) {
                        e.target.querySelector('#email').classList.add('invalid');
                        errorCount += 1;
                        this.setState({formError: validationErrors.email});
                    }
                    else e.target.querySelector('#email').classList.remove('invalid');

                    if(!validateEmailConfirm(formEmail, formEmailConfirm)) {
                        e.target.querySelector('#email_confirm').classList.add('invalid');
                        errorCount += 1;
                        this.setState({formError: validationErrors.email2});
                    }
                    else e.target.querySelector('#email_confirm').classList.remove('invalid');

                    if(errorCount && errorCount > 1) {
                        this.setState({formError: validationErrors.multiple});
                    }
                    if(!errorCount) {
                        this.setState({formError: null, isSending: true});

                        fetch(apiURI, {
                            body: JSON.stringify({name: formFullName, email: formEmail}),
                            method: 'POST',
                            headers : {
                                'Accept': 'application/json'
                            }
                        })
                        .then(() => {
                            this.setState({formError: null, isSending: false, formSuccess: true})
                        })
                        .catch((error) => {
                            error.then(data => this.setState({formError: data.errorMessage, isSending: false}));
                        })
                    }
                }
            }}>
                <Header/>
                <HeroIntro/>
                <Footer/>
                <Overlay>
                    <SubscribeForm/>
                </Overlay>
            </AppContext.Provider>
        )
    }
}


export default App;

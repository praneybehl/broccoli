import React from 'react'
import { OverlayWrap } from './Overlay.style'
import {AppContext} from '../../AppContext'

const Overlay = props => (
    <AppContext.Consumer>
        {(context) => (
        context.state.overlayVisible &&
            <OverlayWrap className="overlay">
                <div className="overlayBox">
                    <a className="closeButton" onClick={context.handleOverlayClose}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                            <path
                                d="M18.984 6.422L13.406 12l5.578 5.578-1.406 1.406L12 13.406l-5.578 5.578-1.406-1.406L10.594 12 5.016 6.422l1.406-1.406L12 10.594l5.578-5.578z"/>
                        </svg>
                    </a>
                    {props.children}
                </div>
            </OverlayWrap>
        )}
    </AppContext.Consumer>
);

export default Overlay;

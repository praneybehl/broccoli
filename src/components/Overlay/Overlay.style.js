import styled from 'react-emotion'

const OverlayWrap = styled('div')`

  background-color: rgba(0, 0, 0, 0.75);
  ${tw('fixed pin flex justify-center items-center')}

  .overlayBox {
    ${tw('block relative bg-white p-6')}
  }

  .closeButton {
    ${tw('absolute w-10 h-10 cursor-pointer p-2 pin-r pin-t')}
  }
  
`;

export { OverlayWrap }

import React from 'react';
import { shallow } from 'enzyme';
import Overlay from './Overlay';

describe('Overlay', () => {
    const overlay = shallow(<Overlay />);

    it('renders correctly', () => { expect(overlay).toMatchSnapshot(); });

});

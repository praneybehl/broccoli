import React from 'react'

import { HeaderWrap } from './Header.styles.js'


const Header = () => (
    <HeaderWrap className="header">
        <div className="container">
            <nav className="navBar">
                <a href="/" className="logo">Broccoli & Co.</a>
            </nav>
        </div>
    </HeaderWrap>
);

export default Header

import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header', () => {
    const header = shallow(<Header />);

    it('renders correctly', () => { expect(header).toMatchSnapshot(); });

    it('renders logo text correctly', () => expect(header.find('.logo').text()).toBe('Broccoli & Co.'))

});

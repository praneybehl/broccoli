import styled from 'react-emotion'

const HeaderWrap = styled('header')`
   
  ${tw('bg-indigo-darker w-full fixed pin-t')}
  .navBar {
    ${tw('flex items-center justify-between flex-wrap p-6')}
  }
  .logo {
    ${tw('no-underline text-xl font-medium text-xl text-white uppercase')}
  }
`;

export { HeaderWrap }

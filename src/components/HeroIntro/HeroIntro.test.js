import React from 'react';
import { shallow } from 'enzyme';
import HeroIntro from './HeroIntro';


describe('HeroIntro', () => {
    const heroIntro = shallow(<HeroIntro />);

    it('renders correctly', () => { expect(heroIntro).toMatchSnapshot(); });

    it('title renders correctly', () => expect(heroIntro.find('.title').text()).toBe('A better way  to enjoy every day.'))

    it('subTitle renders correctly', () => expect(heroIntro.find('.subTitle').text()).toBe('Be the first one to know when we launch.'))

});

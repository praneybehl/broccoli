import React from 'react'
import { HeroIntroWrap } from './HeroIntro.style'
import {AppContext} from '../../AppContext'


const HeroIntro = () => (
    <HeroIntroWrap className="heroIntro">
        <div className="heroIntroInner">
            <h2 className="title">A better way <br/> to enjoy every day.</h2>
            <div className="subTitle">Be the first one to know when we launch.</div>
            <AppContext.Consumer>
                {(context) => <button type="button" className="inviteButton"
                    onClick={context.handleOverlayOpen}>Request an Invite
                </button>}
            </AppContext.Consumer>
        </div>
    </HeroIntroWrap>
)

export default HeroIntro;

import styled from 'react-emotion'

const HeroIntroWrap = styled('section')`
  display: flex;
  padding-top: 71px;
  width: 100%;
  height: calc(100vh - 74px);
  justify-content: center;
  align-items: center;
  
  .heroIntroInner {
    ${tw("w-full md:w-2/5 py-2 md:p-4 block text-center pb-3")}
  }
  .title {
    ${tw("text-indigo-darker text-4xl lg:text-5xl leading-tight tracking-normal mb-4")}
  }
  .subTitle {
    ${tw("text-grey-darker leading-normal font-bold text-base mb-6")}
  }
  .inviteButton {
    transition: background-color 0.3s;
    ${tw("border-indigo-darker border-solid border-2 rounded-sm p-3 hover:bg-indigo-darker hover:text-grey-lightest")}
  }
`;

export { HeroIntroWrap }


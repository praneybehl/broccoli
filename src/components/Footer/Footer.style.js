import styled from 'react-emotion'

const FooterWrap = styled('footer')`${tw("relative bg-indigo-darker text-white /*fixed pin-b pin-x*/ py-6 text-xs")}`;

export { FooterWrap }

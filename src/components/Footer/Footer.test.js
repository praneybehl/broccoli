import React from 'react';
import { shallow } from 'enzyme';
import Footer from './Footer';

describe('Footer', () => {
    const footer = shallow(<Footer />);

    it('renders correctly', () => { expect(footer).toMatchSnapshot(); });

    it('renders text correctly', () => expect(footer.find('.container').text()).toBe('Made with ❤ in Melbourne© 2018. All rights reserved, Broccoli & Co.'))

});

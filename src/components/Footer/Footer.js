import React from 'react'
import { FooterWrap } from './Footer.style'

const Footer = () => (
    <FooterWrap className="footer" >
        <div className="container text-center">
            <div>Made with &#10084; in Melbourne</div>
            <div>&copy; 2018. All rights reserved, Broccoli & Co.</div>
        </div>
    </FooterWrap>
)

export default Footer

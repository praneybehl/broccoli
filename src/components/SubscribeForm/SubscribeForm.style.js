import styled from 'react-emotion'

const FormContainer = styled('div')`
    display: block;
    width: 100%;
    min-width: 300px;
    padding: 40px 0;
  
  .formTitle {
    ${tw('block w-full font-bold text-center text-indigo-darker')};
  }
  
  .keyline {
    background-color: #2F365F;
    display: block;
    height: 2px;
    width: 32px;
    margin: 20px auto 40px auto;
  }

  .inputWrap {
    ${tw('flex flex-col mb-4')}
  }

  .inputField {
    ${tw('border border-indigo-darker p-3 text-grey-darkest rounded-sm')}
    
    &.invalid {
      ${tw('border-red')}
    }
    }
  }

  .button {
    margin-top: 40px;
    transition: background-color 0.3s;
    ${tw('w-full border border-indigo-darker block bg-teal-lightest hover:bg-teal-lighter text-indigo-darker p-3 rounded-sm')}
  }

  .error {
    ${tw('text-red font-bold mx-auto p-2 text-center')}
  }

  .success {
    ${tw('text-grey-darker text-center')}
  }
`;


export { FormContainer }

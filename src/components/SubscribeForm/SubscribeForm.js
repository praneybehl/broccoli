import React from 'react'
import { FormContainer } from './SubscribeForm.style'
import { AppContext } from '../../AppContext'

const SubscribeForm =  props => (
    <AppContext.Consumer>
        {
            (context) => <FormContainer className="formContainer">
                <form onSubmit={context.handleFormSubmit}>
                    <h3 className="formTitle">{context.state.formSuccess ? context.state.formTitleSuccessText : context.state.formTitleText}</h3>

                    <div className="keyline"></div>

                    { context.state.formSuccess ? (
                            <div className="success">You will be one of the first to experience <br/> Broccoli & Co. when we launch.</div>
                        ) :
                        (<React.Fragment>
                            <div className="inputWrap">
                                <input className="inputField" type="text" name="full_name"
                                       id="full_name" placeholder="Full Name"
                                       onChange={context.updateInputField}/>
                            </div>

                            <div className="inputWrap">
                                <input className="inputField" type="email" name="email"
                                       id="email" placeholder="Email"
                                       onChange={context.updateInputField}/>
                            </div>

                            <div className="inputWrap">
                                <input className="inputField" type="email" name="email_confirm"
                                       id="email_confirm" placeholder="Confirm Email"
                                       onChange={context.updateInputField}/>
                            </div>
                        </React.Fragment>)
                    }

                    { context.state.formSuccess ?
                        <button className="button" type="button"
                                onClick={context.handleOverlayClose}>Ok</button>
                        :
                        <button className="button" type="submit" disabled={context.state.isSending}>
                            {!context.state.isSending ?
                                context.state.formCtaText :
                                context.state.formSendingText}
                        </button>
                    }

                    { context.state.formError && <div className="error">{context.state.formError}</div>}

                </form>
            </FormContainer>
        }
    </AppContext.Consumer>
);

export default SubscribeForm;

import React from 'react';
import { shallow } from 'enzyme';
import SubscribeForm from './SubscribeForm';

describe('SubscribeForm', () => {
    const subscribeform = shallow(<SubscribeForm />);

    it('renders correctly', () => { expect(subscribeform).toMatchSnapshot(); });

});

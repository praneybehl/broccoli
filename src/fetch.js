export default (uri, options = {}) => {

    return new Promise((resolve, reject) => {

        options.headers = {
            'Accept': 'application/json'
        };
        fetch(uri, options)

            .then(response => {

                if (response.status === 200) {

                    return resolve(response.json());

                } else {

                    return reject(response.json());

                }
            });
    });

}

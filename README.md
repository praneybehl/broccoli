#Broccoli

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Technology Stack:
* React
* React Context API
* Emotion.js
* Tailwindcss
* Webpack
* Jest w/Enzyme (Unit testing)
* Yarn

---

## Prerequisites
### All
* Yarn
* Node v8.x.x & Npm v5.x.x
* NVM (optional but makes the above easier)


## Installation and running the application
* install using `yarn install`
* build and run using `yarn start`
* unit test using `yarn test` 
